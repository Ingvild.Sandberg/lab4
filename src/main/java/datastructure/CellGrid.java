package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	//number of rows and columns 
	int rows;
	int columns;
	CellState initial;
	CellState[][] grid; //the grid 

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows; 
		this.columns = columns; 
		this.grid = new CellState[rows][columns];
		
		//sets value of every cell to initial state 
		for (int i = 0; i < rows; i++) { //rows 
			for (int j = 0; j < columns; j++) { //columns
				grid[i][j] = initialState;
			}
		}
		
		
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	if (0 <= row && row < this.rows && 0 <= column && column < this.columns) {
    		grid[row][column] = element;
    	} else {
    		throw new IndexOutOfBoundsException("Index out of bounds"); 
    	}
    }

    @Override
    public CellState get(int row, int column) {
        if (0 <= row && 0 <= column && row < this.rows && column < this.columns) {
        	return grid[row][column];
        } else {
        	throw new IndexOutOfBoundsException("Index out of bounds");
        }
        
    }

    @Override
    public IGrid copy() {
    	IGrid myCopy = new CellGrid(this.rows, this.columns, this.initial);
    	
    	for (int therow = 0; therow < this.rows; therow++) {
    		for (int thecolumn = 0; thecolumn < this.columns; thecolumn++) {
    			myCopy.set(therow, thecolumn, get(therow, thecolumn));
    		}
    	}
        return myCopy;
    }
    
}
