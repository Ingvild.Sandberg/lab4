package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Brians brain Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}
	
	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}
	
	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}
	
	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		
		for (int therow = 0; therow < numberOfRows(); therow++) {
			for(int thecolumn = 0; thecolumn < numberOfColumns(); thecolumn++) {
				nextGeneration.set(therow, thecolumn, getNextCell(therow, thecolumn));
			}
		}
		
		currentGeneration = nextGeneration;
	} 
	
	public CellState getNextCell(int row, int col) {
		int livingNeighbours = countNeighbors(row, col, CellState.ALIVE);
		CellState newState = getCellState(row,col);
		
		if (getCellState(row,col) == CellState.ALIVE) {
			newState = CellState.DYING;
		}
		
		if (getCellState(row,col) == CellState.DYING) {
			newState = CellState.DEAD;
		}
		if (getCellState(row,col) == CellState.DEAD) {
			if (livingNeighbours == 2) {
				newState = CellState.ALIVE;
			} else {
				newState = CellState.DEAD;
			}
		}
				
		return newState;
		
	}
	
	private int countNeighbors(int row, int col, CellState state) {
		int number = 0; 
		
		for (int i = 0; i < numberOfRows(); i++) {
			for (int j = 0; j < numberOfColumns(); j++) {
				
				if (i == row - 1 && j == col - 1) {
					if (getCellState(i,j) == state) {
						number++;
					}
				}
				if (i == row - 1 && j == col) {
					if (getCellState(i,j) == state) {
						number++;
					}
				}
				if (i == row - 1 && j == col + 1) {
					if (getCellState(i,j) == state) {
						number++;
					}
				}
				if (i == row && j == col - 1) {
					if (getCellState(i,j) == state) {
						number++;
					}
				}
				if (i == row && j == col + 1) {
					if (getCellState(i,j) == state) {
						number++;
					}
				}
				if (i == row + 1 && j == col - 1) {
					if (getCellState(i,j) == state) {
						number++;
					}
				}
				if (i == row + 1 && j == col) {
					if (getCellState(i,j) == state) {
						number++;
					}
				}
				if (i == row + 1 && j == col + 1) {
					if (getCellState(i,j) == state) {
						number++;
					}
				}
				
			}
		}
		
		return number;
	}
	
	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
	
	
	
}
